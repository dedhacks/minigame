package com.jcardonne.minigame.listener;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class InArenaListener implements Listener {

	@EventHandler
	public void onQuit(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final Arena arena = ArenaManager.findArena(player);

		if (arena != null)
			arena.leavePlayer(player);
	}
}
