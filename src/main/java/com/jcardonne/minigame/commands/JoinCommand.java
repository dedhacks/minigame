package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaJoinMode;

public class JoinCommand extends ArenaSubCommand {

	public JoinCommand() {
		super("j|join", 1, "<arena>", "Go play an arena");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Arena arena = findArena(args[0]);

		arena.joinPlayer(getPlayer(), ArenaJoinMode.PLAYING);
	}
}
