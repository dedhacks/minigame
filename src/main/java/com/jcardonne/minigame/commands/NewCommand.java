package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaManager;
import com.jcardonne.minigame.monster.MobArena;

import java.util.ArrayList;
import java.util.List;

public class NewCommand extends ArenaSubCommand {

	protected NewCommand() {
		super("new|n", 1, "<arena>", "Create a new arena");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		final String name = args[0];
		checkArenaNotLoaded(name);
		final Arena arena = ArenaManager.loadOrCreateArena(name, MobArena.TYPE);
		tellSuccess("Arena " + arena.getName() + " has been successfully created.");
	}

	@Override
	protected List<String> tabComplete() {
		return new ArrayList<>();
	}
}
