package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaManager;
import com.jcardonne.minigame.model.ArenaPlayer;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.collection.StrictList;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.command.SimpleSubCommand;

import java.util.List;

public abstract class ArenaSubCommand extends SimpleSubCommand {


	protected ArenaSubCommand(final String sublabel, String description) {
		this(sublabel, 0, "", description);
	}

	protected ArenaSubCommand(final String sublabel, int minArgs, String usage, String description) {
		super(sublabel);

		setMinArguments(minArgs);
		setUsage(usage);
		setDescription(description);
	}
	
	protected final Arena findArena(String name) {
		Arena arena = ArenaManager.findArena( name);
		checkNotNull(arena, Common.format("Arena %s does not existe. Available %s", name, ArenaManager.getArenaNames()));

		return arena;
	}

	protected final void checkInArena() {
		checkConsole();

		ArenaPlayer cache = ArenaPlayer.getCache(getPlayer());
		checkBoolean(cache.hasArena(), "Your are not joined in any arena");
	}

	protected final void checkNotInArena() {
		checkConsole();

		ArenaPlayer cache = ArenaPlayer.getCache(getPlayer());

		if (!cache.hasArena())
			returnTell("You are already joined in arena " + cache.getArena().getName() + ".");
	}

	protected final void checkArenaLoaded(String name) {
		checkBoolean(ArenaManager.isArenaLoaded(name),Common.format("Arena %s does not exist.", name));
	}

	protected final void checkArenaNotLoaded(String name) {
		checkBoolean(!ArenaManager.isArenaLoaded(name),Common.format("Arena %s already exist.", name));
	}

	@Override
	protected List<String> tabComplete() {
		return completeLastWord(ArenaManager.getArenaNames());
	}
}
