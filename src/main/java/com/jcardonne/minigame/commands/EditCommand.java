package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaJoinMode;
import com.jcardonne.minigame.model.ArenaPlayer;
import org.mineacademy.fo.Common;

public class EditCommand extends ArenaSubCommand {

	public EditCommand() {
		super("edit|e", 1, "<arena>", "Edit an existing arena.");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		ArenaPlayer arenaPlayer = ArenaPlayer.getCache(getPlayer());
		Arena arena = findArena(args[0]);

		if (arenaPlayer.hasArena()) {
			if (arenaPlayer.getArena().equals(arena) && arenaPlayer.getMode() == ArenaJoinMode.EDITING) {
				arena.leavePlayer(getPlayer());

				tellInfo("You're no longer editing arena {0}");
				return;
			}

			returnTell("Stop " + arenaPlayer.getMode().getLocalized() + " the arena " + arenaPlayer.getArena().getName() + " before editing this.");
		}

		if (arena.joinPlayer(getPlayer(), ArenaJoinMode.EDITING)) {
			tellInfo("You're now editing arena {0}. " + (arena.getPlayers().size() < 2 ? "" : "Other editors: " + Common.joinPlayers(arena.getPlayers())));
		}
	}
}
