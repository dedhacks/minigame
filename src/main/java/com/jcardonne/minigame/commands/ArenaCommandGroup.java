package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.MiniGamePlugin;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class ArenaCommandGroup extends SimpleCommandGroup {

	@Override
	protected void registerSubcommands() {

		for (final Class<? extends ArenaSubCommand> clazz : ReflectionUtil.getClasses(MiniGamePlugin.getInstance(), ArenaSubCommand.class)) {
			Common.log("Registering command " + clazz.getSimpleName());

			registerSubcommand(ReflectionUtil.instantiate(clazz));
		}
	}
}
