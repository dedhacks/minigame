package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.tools.ArenaTools;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.MenuTools;

import java.util.ArrayList;
import java.util.List;

public class ToolsCommand extends ArenaSubCommand {

	protected ToolsCommand() {
		super("tools|t", "Goodies for editing arena.");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Menu menu = MenuTools.of(ArenaTools.class, "Use these goodies to fancy", "up your arenas with ease!");
		menu.displayTo(getPlayer());
	}

	@Override
	protected List<String> tabComplete() {
		return new ArrayList<>();
	}
}
