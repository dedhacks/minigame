package com.jcardonne.minigame.commands;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaManager;

import java.util.ArrayList;
import java.util.List;

public class LeaveCommand extends ArenaSubCommand {

	protected LeaveCommand() {
		super("l|leave", "Go play an arena");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		checkInArena();

		final Arena arena = ArenaManager.findArena(getPlayer());
		arena.leavePlayer(getPlayer());

		tellWarn("You have left " + arena.getName() + ".");
	}

	@Override
	protected List<String> tabComplete() {
		return new ArrayList<>();
	}
}
