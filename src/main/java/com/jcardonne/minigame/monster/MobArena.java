package com.jcardonne.minigame.monster;

import com.jcardonne.minigame.model.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.List;

public class MobArena extends Arena {

	public static final String TYPE = "monster"; //ignore field from obfuscation


	//Always have a single public constructor taking in arena name
	public MobArena(final String name) {
		super(TYPE, name);
	}

	@Override
	protected ArenaSettings createSettings(final String name) {
		return new ArenaSettings(name);
	}

	@Override
	protected ArenaHeartbeat createHeartbeat() {
		return new MobArenaHeartBeat(this);
	}

	@Override
	protected ArenaScoreboard createScoreboard() {
		return new MobArenaScoreboard(this);
	}

	@Override
	protected void onStart() {
		super.onStart();

		forEach(player -> teleport(player, getSettings().getEntrancelocation()));
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (getState() == ArenaState.PLAYED) {
			cleanEntities();
		}
	}

	private void cleanEntities() {
		final List<Entity> entities = getSettings().getRegion().getEntities();

		for (final Entity entity : entities)
			if (entity.getType() != EntityType.PLAYER)
				entity.remove();

	}

	@Override
	public MobArenaHeartBeat getHeartbeat() {
		return (MobArenaHeartBeat) super.getHeartbeat();
	}

	@Override
	public MobArenaSettings getSettings() {
		return (MobArenaSettings) super.getSettings();
	}

	@Override
	public MobArenaScoreboard getScoreboard() {
		return (MobArenaScoreboard) super.getScoreboard();
	}

	@Override
	protected void onJoin(final Player player, final ArenaJoinMode joinMode) {
		super.onJoin(player, joinMode);
	}


}
