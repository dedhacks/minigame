package com.jcardonne.minigame.monster;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaHeartbeat;
import lombok.Getter;
import org.bukkit.Location;

public class MobArenaHeartBeat extends ArenaHeartbeat {

	@Getter
	private int wave = 0;

	protected MobArenaHeartBeat(final Arena arena) {
		super(arena);
	}

	@Override
	protected void onStart() {
		super.onStart();

		wave = 1;
		tickSpawnpoints();
	}

	@Override
	protected void onTick() {
		super.onTick();

		final int elapsedSeconds = getCountdownSeconds() - getTimeLeft();
		final int waveDuration = getArena().getSettings().getWaveDuration().getTimeSeconds();

		// ex: 10 seconds duration
		// 3 seconds Long
		// => 1,2,3 - new wave 4,5,6 - new wave 7,8,9 - new wave

		if (elapsedSeconds % waveDuration == 0 && elapsedSeconds + 1 < getCountdownSeconds()) {
			wave++;

			onNextWave();
		}
	}

	private void onNextWave() {
		getArena().broadcastInfo("Arena entered the " + wave + " wave!");
		tickSpawnpoints();
	}

	private void tickSpawnpoints() {
		for (final MobArenaSettings.MobSpawnpoint point : getArena().getSettings().getMobSpawnpoints()) {
			final Location location = point.getLocation().clone().add(0.5, 1, 0.5); // Spawns on the top&middle of the block

			location.getWorld().spawnEntity(location, point.getEntity());
		}
	}

	@Override
	public MobArena getArena() {
		return (MobArena) super.getArena();
	}

	@Override
	protected void onEnd() {
		super.onEnd();

		wave = 0;
	}
}
