package com.jcardonne.minigame.monster;

import com.jcardonne.minigame.model.ArenaSettings;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.ConfigSerializable;
import org.mineacademy.fo.model.SimpleTime;

import java.util.List;

@Getter
public class MobArenaSettings extends ArenaSettings {

	private SimpleTime waveDuration;
	private Location entrancelocation;
	private List<MobSpawnpoint> mobSpawnpoints;

	public MobArenaSettings(final String arenaName) {
		super(arenaName);
	}

	@Override
	protected void onLoadFinish() {
		super.onLoadFinish();

		this.waveDuration = getTime("Wave_Duration", "4 seconds");
		this.entrancelocation = getLocation("Entrance_Location");
		this.mobSpawnpoints = getList("Monster_Spawnpoints", MobSpawnpoint.class, this);
	}

	public void setWaveDuration(final SimpleTime waveDuration) {
		this.waveDuration = waveDuration;
		save();
	}

	public void setEntrancelocation(final Location entrancelocation) {
		this.entrancelocation = entrancelocation;
		save();
	}

	public boolean toggleMobSpawnPoint(final Location location, final EntityType type) {
		for (final MobSpawnpoint point : mobSpawnpoints)
			if (Valid.locationEquals(point.getLocation(), location)) {
				mobSpawnpoints.remove(point);
				save();
				return false; //means the point has existed and now is removed
			}

		mobSpawnpoints.add(new MobSpawnpoint(this, location, type));
		save();
		return true; //means the point as been added
	}


	public MobSpawnpoint findMobSpawnpoint(final Location location) {
		for (final MobSpawnpoint point : mobSpawnpoints)
			if (Valid.locationEquals(point.getLocation(), location))
				return point;

		return null;
	}

	public final void addMobSpawnpoint(final Location location, final EntityType type) {
		Valid.checkBoolean(findMobSpawnpoint(location) == null, "Monster spawn point already exists at " + Common.shortLocation(location));

		mobSpawnpoints.add(new MobSpawnpoint(this, location, type));
		save();
	}

	public void removeMobSpawnPoint(final Location location) {
		final MobSpawnpoint point = findMobSpawnpoint(location);

		Valid.checkNotNull(point, "Monster spawn point does not exists at " + Common.shortLocation(location));
		mobSpawnpoints.remove(point);
		save();
	}

	@Override
	public boolean isSetup() {
		return super.isSetup() && entrancelocation != null;
	}


	// --------------------------------------------------------------------------------------------------------------
	// Classes
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Represents a simple spawn point
	 */
	@Getter
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class MobSpawnpoint implements ConfigSerializable {

		/**
		 * The settings associated with this class
		 */
		private final MobArenaSettings settings;

		/**
		 * The location of this spawner
		 */
		private Location location;

		/**
		 * The monster being spawned
		 */
		private EntityType entity;

		/**
		 * Set a new location
		 *
		 * @param location the location to set
		 */
		public void setLocation(final Location location) {
			this.location = location;

			settings.save();
		}

		/**
		 * Set the entity
		 *
		 * @param entity the entity to set
		 */
		public void setEntity(final EntityType entity) {
			this.entity = entity;

			settings.save();
		}

		/**
		 * Automatically turn saved config value into this class
		 *
		 * @param map
		 * @param settings
		 * @return
		 */
		public static MobSpawnpoint deserialize(final SerializedMap map, final MobArenaSettings settings) {
			final MobSpawnpoint point = new MobSpawnpoint(settings);

			point.location = map.getLocation("Location");
			point.entity = map.get("Entity", EntityType.class);

			return point;
		}

		/**
		 * Convert this class into a saveable config section
		 *
		 * @see org.mineacademy.fo.model.ConfigSerializable#serialize()
		 */
		@Override
		public SerializedMap serialize() {
			return SerializedMap.ofArray(
					"Location", location,
					"Entity", entity);
		}
	}
}
