package com.jcardonne.minigame.monster;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaScoreboard;
import org.mineacademy.fo.model.Replacer;

public class MobArenaScoreboard extends ArenaScoreboard {

	public MobArenaScoreboard(final Arena arena) {
		super(arena);
	}

	@Override
	protected String replaceVariablesLate(String message) {
		final MobArenaSettings settings = (MobArenaSettings) getArena().getSettings();

		message = Replacer.of(message).replaceAll(
				"wave", getArena().getHeartbeat().getWave(),
				"mob_spawnpoint_set", settings.getMobSpawnpoints().size(),
				"spawnpoint_set", settings.getEntrancelocation() != null);

		return message;
	}

	@Override
	public MobArena getArena() {
		return (MobArena) super.getArena();
	}

	@Override
	protected void addEditRows() {
		addRows(
				"Spawnpoint _{spawnpoint_set}",
				"Mob spawnspoints: {mob_spawnpoint_set}"
		);
	}

	@Override
	public void onStart() {
		super.onStart();

		addRows("Wave: {wave}");
	}
}
