package com.jcardonne.minigame.tools;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaSettings;
import com.jcardonne.minigame.monster.MobArena;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

public class ToolLobby extends ArenaTools<Arena> {

	private static final Tool instance = new ToolLobby();

	private ToolLobby() {
		super(Arena.class);
	}

	@Override
	protected void onBlockClick(final Player player, final Arena arena, final ClickType clickType, final Block block) {
		ArenaSettings settings = arena.getSettings();
		Location location = block.getLocation();

		location.setYaw(player.getLocation().getYaw());
		location.setPitch(0); //prob not have to be set

		settings.setLobby(location);
		Messenger.success(player, "Set the lobby arena point.");
	}

	@Override
	protected boolean autoCancel() {
		return true; //Do not break/place blocks
	}

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.DIAMOND_PICKAXE, "&lLOBBY TOOL", "", "Right Click to place", "arena lobby point.").build().makeMenuTool();

	}
}
