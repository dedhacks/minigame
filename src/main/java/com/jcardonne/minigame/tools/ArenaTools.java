package com.jcardonne.minigame.tools;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaJoinMode;
import com.jcardonne.minigame.model.ArenaPlayer;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.tool.BlockTool;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class ArenaTools<T extends Arena> extends BlockTool {

	private final Class<T> arenaType;

	protected ArenaTools() {
		this(null);
	}

	@Override
	protected final void onBlockClick(final Player player, final ClickType click, final Block block) {
		ArenaPlayer arenaPlayer = ArenaPlayer.getCache(player);

		if (arenaPlayer.getMode() != ArenaJoinMode.EDITING) {
			Messenger.error(player,"To use this tool select an arena to edit with /arena edit first.");

			return;
		}

		if (arenaType != null && !arenaType.isAssignableFrom(arenaPlayer.getArena().getClass())) {
			Messenger.error(player, "This tool is not compatible with this arena");
		}

		onBlockClick(player, (T) arenaPlayer.getArena(), click, block);
	}

	protected abstract void onBlockClick(Player player, T arena, ClickType clickType, Block block);
}
