package com.jcardonne.minigame.tools;

import com.jcardonne.minigame.model.Arena;
import com.jcardonne.minigame.model.ArenaSettings;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

public class ToolRegion extends ArenaTools<Arena> {

	private static final Tool instance = new ToolRegion();

	private ToolRegion() {
		super(Arena.class);
	}

	@Override
	protected void onBlockClick(final Player player, final Arena arena, final ClickType clickType, final Block block) {
		final ArenaSettings settings = arena.getSettings();
		final Location location = block.getLocation();
		final boolean primary = clickType == ClickType.LEFT;

		if (primary)
			settings.setRegion(location, null);
		else
			settings.setRegion(null, location);

		Messenger.success(player, "Set the " + (primary ? "primary" : "secondary") + " arena point.");
	}

	@Override
	protected boolean autoCancel() {
		return true; //Do not break/place blocks
	}

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.DIAMOND_SHOVEL, "&lREGION TOOL", "", "Right Click to set primary", "Left Click to set secondary").build().makeMenuTool();

	}
}
