package com.jcardonne.minigame.tools;

import com.jcardonne.minigame.monster.MobArena;
import com.jcardonne.minigame.monster.MobArenaSettings;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

public class ToolEntrance extends ArenaTools<MobArena> {

	private static final Tool instance = new ToolEntrance();

	private ToolEntrance() {
		super(MobArena.class);
	}

	@Override
	protected void onBlockClick(final Player player, final MobArena arena, final ClickType clickType, final Block block) {
		final MobArenaSettings settings = arena.getSettings();
		final Location location = block.getLocation();

		location.setYaw(player.getLocation().getYaw());
		location.setPitch(0); //prob not have to be set

		settings.setEntrancelocation(location);
		Messenger.success(player, "Set the arena entrance point for players.");
	}

	@Override
	protected boolean autoCancel() {
		return true; //Do not break/place blocks
	}

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.DIAMOND_AXE, "&lENTRANCE TOOL", "", "Right Click to place", "arena entrance point.").build().makeMenuTool();

	}
}
