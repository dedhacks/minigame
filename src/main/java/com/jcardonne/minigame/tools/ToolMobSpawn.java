package com.jcardonne.minigame.tools;

import com.jcardonne.minigame.monster.MobArena;
import com.jcardonne.minigame.monster.MobArenaSettings;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

public class ToolMobSpawn extends ArenaTools<MobArena> {

	private static final Tool instance = new ToolMobSpawn();

	private ToolMobSpawn() {
		super(MobArena.class);
	}

	@Override
	protected void onBlockClick(final Player player, final MobArena arena, final ClickType clickType, final Block block) {
		final MobArenaSettings settings = arena.getSettings();
		final boolean added = settings.toggleMobSpawnPoint(block.getLocation(), EntityType.ZOMBIE); //TODO

		Messenger.success(player, "Successfuly " + (added ? "&2added&7" : "&cremoved&7") + " a monster spawn point.");
	}

	@Override
	protected boolean autoCancel() {
		return true; //Do not break/place blocks
	}

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.DIAMOND_SWORD, "&lMobSpawn TOOL", "", "Right Click to spawn", "mobs.").build().makeMenuTool();

	}
}
