package com.jcardonne.minigame;

import com.jcardonne.minigame.commands.ArenaCommandGroup;
import com.jcardonne.minigame.listener.InArenaListener;
import com.jcardonne.minigame.model.ArenaManager;
import com.jcardonne.minigame.monster.MobArena;
import com.jcardonne.minigame.settings.Settings;
import lombok.Getter;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.settings.YamlStaticConfig;

import java.util.Arrays;
import java.util.List;

public class MiniGamePlugin extends SimplePlugin {

	@Getter
	private final SimpleCommandGroup mainCommand = new ArenaCommandGroup();

	@Override
	protected void onPluginPreStart() {
		ArenaManager.registerArenaType(MobArena.class);

		//Messenger.ERROR_PREFIX = "[!] ";
	}

	@Override
	protected void onPluginStart() {
		Common.ADD_TELL_PREFIX = true;
		Common.setTellPrefix("[Arena]");

		SimpleCommand.USE_MESSENGER = true;
	}

	@Override
	protected void onReloadablesStart() {
		ArenaManager.loadArenas();
		registerEvents(new InArenaListener());
	}

	@Override
	protected void onPluginReload() {
		ArenaManager.stopArenas();
	}

	@Override
	protected void onPluginStop() {
		ArenaManager.stopArenas();
	}

	@Override
	public List<Class<? extends YamlStaticConfig>> getSettings() {
		return Arrays.asList(Settings.class);
	}

}
