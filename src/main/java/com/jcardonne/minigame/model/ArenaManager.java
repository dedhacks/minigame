package com.jcardonne.minigame.model;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.FileUtil;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.ReflectionUtil.ReflectionException;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.StrictMap;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.region.Region;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@UtilityClass
public class ArenaManager {

	private final StrictMap<String, Class<? extends Arena>> registeredTypes = new StrictMap<>();

	private final List<Arena> loadedArenas = new ArrayList<>();

	public void registerArenaType(final Class<? extends Arena> clazz) {
		final String type;
		//get arena type from another classes
		try {
			type = ReflectionUtil.getStaticFieldContent(clazz, "TYPE");

		} catch (final ReflectionException ex) {
			throw new FoException("Please put 'public static String TYPE' with the unique arena type to " + clazz);
		}
		registeredTypes.put(type, clazz);
	}

	public void clearRegisteredArenaTypes() {
		registeredTypes.clear();
	}

	public void loadArenas() {
		loadedArenas.clear();

		final File[] arenaFiles = FileUtil.getFiles("arenas", "yml");

		for (final File arenaFile : arenaFiles) {
			final String name = FileUtil.getFileName(arenaFile);
			final String type = detectArenaType(arenaFile);

			loadOrCreateArena(name, type);
		}
	}

	private String detectArenaType(final File file) {
		final YamlConfiguration config = FileUtil.loadConfigurationStrict(file);
		final String type = config.getString("Type");

		Valid.checkNotNull(type, "Make some arena type in " + file);
		return type;
	}

	public void stopArenas() {
		for (final Arena arena : loadedArenas)
			if (!arena.isStopped())
				arena.stopArena();
	}

	public Arena loadOrCreateArena(final String name, @NonNull final String type) {
		final Class<? extends Arena> arenaClass = registeredTypes.get(type);
		Valid.checkNotNull(arenaClass, "Arena type " + type + " not supported. Available: " + registeredTypes.keySet());
		Valid.checkBoolean(!isArenaLoaded(name), "Arena " + name + " is already loaded: " + getArenaNames());

		try {
			final Arena arena = ReflectionUtil.instantiate(arenaClass, name);
			loadedArenas.add(arena);

			Common.log("[+] Loaded " + type + " arena " + arena.getName());
			return arena;

		} catch (final ReflectionException ex) {
			Common.throwError(ex, "Failed to create arena type " + type + ", ensure that " + arenaClass + " has 1 public constructor only taking the arena name");

		} catch (final Throwable t) {
			Common.throwError(t, "Failed to load arena " + name + " of type " + type);
		}

		return null;
	}

	public void removeArena(final String name) {
		final Arena arena = findArena(name);
		Valid.checkNotNull(arena, "Cannot remove a non-existing arena: " + name);

		arena.getSettings().delete();
		loadedArenas.remove(arena);
	}

	public boolean isArenaLoaded(final String name) {
		return findArena(name) != null;
	}

	public Arena findArena(@NonNull final String name) {
		for (final Arena arena : loadedArenas) {
			if (arena.getName().equalsIgnoreCase(name)) {
				return arena;
			}
			return null;
		}
		return null;
	}

	public Arena findArena(final Location location) {
		for (final Arena arena : loadedArenas) {
			final Region region = arena.getSettings().getRegion();

			if (region != null && region.isWhole() && region.isWithin(location))
				return arena;
		}
		return null;
	}

	public Arena findArena(@NonNull final Player player) {
		final ArenaPlayer cache = ArenaPlayer.getCache(player);

		return cache.hasArena() ? cache.getArena() : null;
	}

	public List<Arena> getArenas() {
		return Collections.unmodifiableList(loadedArenas);
	}

	public List<String> getArenaNames() {
		return Common.convert(loadedArenas, Arena::getName);
	}

}