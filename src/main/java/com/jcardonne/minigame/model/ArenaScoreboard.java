package com.jcardonne.minigame.model;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.Replacer;
import org.mineacademy.fo.model.SimpleScoreboard;

public class ArenaScoreboard extends SimpleScoreboard {

	@Getter
	private final Arena arena;

	public ArenaScoreboard(Arena arena) {
		this.arena = arena;

		this.setTitle("&8- &7Arena &c" + arena.getName() + " &8-");
		this.setTheme(ChatColor.RED, ChatColor.GRAY);
		this.setUpdateDelayTicks(40); //2 seconds
	}

	//@Override
	protected String replaceVariables(String message) {
		ArenaSettings settings = arena.getSettings();



		message = Replacer.of(message).replaceAll(
				"players", arena.getPlayers().size(),
				"remaining_start", Common.plural(arena.getStartCountdown().getTimeLeft(), "second"),
				"remaining_end", Common.plural(arena.getHeartbeat().getTimeLeft(), "second"),
				"state", arena.getState().getLocalized(),
				"lobby_set", settings.getLobbyLocation() != null,
				"region_set", settings.getRegion() != null && settings.getRegion().isWhole()
		);

		message = replaceVariablesLate(message);

		return message.replace("_true", "&ayes").replace("_false", "&cno"); //"region_set" will return "_true"
	}

	protected String replaceVariablesLate(String message) {
		return message;
	}

	public void onPLayerJoin(Player player, ArenaJoinMode joinMode) {
		show(player);
	}

	public void onPlayerLeave(Player player) {
		if (isViewing(player))
			hide(player);
	}

	public void onLobbyStart() {
		addRows("",
				"Players: {players}",
				"Time to start: {remaining_start}"
				);
	}

	public void onEditStart() {
		addRows("",
				"Editing players: {players}",
				"",
				"Lobby: _{lobby_set}",
				"Region: _{region_set}"
				);
		addEditRows();
		addRows("",
				"&7Use: /arena tools to edit.");
	}

	protected void addEditRows() {

	}

	public void onStart() {
		removeRow("Time to start");
				addRows("Time left: {remaining_end");
	}

	public void onStop(){
		clearRows();

		stop();
	}


}
