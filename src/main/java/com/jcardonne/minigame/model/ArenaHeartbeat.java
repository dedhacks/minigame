package com.jcardonne.minigame.model;

import lombok.Getter;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.Countdown;

public class ArenaHeartbeat extends Countdown {

	@Getter
	private final Arena arena;

	protected ArenaHeartbeat(final Arena arena) {
		super(arena.getSettings().getGameDuration());

		this.arena = arena;
	}

	@Override
	protected void onStart() {
	}

	@Override
	protected void onTick() { //run every second until it hit 0
		arena.broadcastWarning("Arena ends in less than " + Common.plural(getTimeLeft(), " second") + ".");
	}

	@Override
	protected void onEnd() {
		arena.stopArena();
	}
}
