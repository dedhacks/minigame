package com.jcardonne.minigame.model;

import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.Valid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public abstract class Arena {

	//TODO Arena Settings -> YAML CONFIG

	private final String name;
	private final String type; //bedwars|mob|spleef
	private final ArenaSettings settings;

	private final List<ArenaPlayer> players = new ArrayList<>();

	private final ArenaCountdownStart startCountdown;

	private final ArenaHeartbeat heartbeat; //playing, spect, reload etc.

	private final ArenaScoreboard scoreboard;

	private ArenaState state = ArenaState.STOPPED;

	private boolean stopping = false;

	public Arena(final String type, final String name) {
		this.type = type;
		this.name = name;
		this.settings = createSettings(name);
		this.startCountdown = new ArenaCountdownStart(this);
		this.heartbeat = createHeartbeat();
		this.scoreboard = createScoreboard();

		this.settings.setArenaType(type);
	}

	protected abstract ArenaSettings createSettings(String name);

	protected ArenaHeartbeat createHeartbeat() {
		return new ArenaHeartbeat(this);
	}

	protected ArenaScoreboard createScoreboard() {
		return new ArenaScoreboard(this);
	}

	//----------------------------------------------------
	//              basic player logic
	//----------------------------------------------------

	//return true if the player has joined
	public final boolean joinPlayer(final Player player, final ArenaJoinMode joinMode) {
		final ArenaPlayer cache = ArenaPlayer.getCache(player.getUniqueId());

		if (!canJoin(player, joinMode))
			return false;

		cache.markArenaJoin(player, this, joinMode);
		players.add(cache);

		try {
			onJoin(player, joinMode);
		} catch (final Throwable t) {
			Common.error(t, "Failed to properly handle " + player.getName() + " " + joinMode.getLocalized() + " arena " + name + ", aborting.");

			players.remove(cache);

			cache.markArenaLeft();
			return false;
		}

		if (state == ArenaState.STOPPED) {
			if (joinMode == ArenaJoinMode.EDITING) {
				state = ArenaState.EDITED;

				onEditStart();
			} else {
				state = ArenaState.LOBBY;

				onLobbyStart();
			}
			checkIntegrity();
		}

		onJoin(player, joinMode);
		return true;
	}

	//condition to properly join an arena
	protected boolean canJoin(final Player player, final ArenaJoinMode joinMode) {
		final ArenaPlayer cache = ArenaPlayer.getCache(player.getUniqueId());

		if (cache.hasArena()) {
			Messenger.error(player, "You are already " + cache.getMode().getLocalized() + cache.getArena().getName());
			return false;
		}

		if (state == ArenaState.EDITED && joinMode != ArenaJoinMode.EDITING) {
			Messenger.error(player, "Arena is being edited right now");
			return false;
		}

		if (state != ArenaState.EDITED && state != ArenaState.STOPPED && joinMode == ArenaJoinMode.EDITING) {
			Messenger.error(player, "Arenas thare are running cannot be edited");
			return false;
		}

		if (state != ArenaState.PLAYED && joinMode == ArenaJoinMode.SPECTATING) {
			Messenger.error(player, "You cannot spectate stopped arena");
			return false;
		}

		if (state == ArenaState.PLAYED && joinMode == ArenaJoinMode.PLAYING) {
			Messenger.error(player, "The arena has already begun.");

			return false;
		}

		if (!isReady() && joinMode != ArenaJoinMode.EDITING) {
			Messenger.error(player, "Arena not yet configured. Use /arena edit to see what's missing");
			return false;
		}

		if (joinMode == ArenaJoinMode.PLAYING && players.size() >= settings.getMaxPlayers()) {
			Messenger.error(player, "Arena not yet configured. Use /arena edit to see what's missing");

			return false;
		}

		return true;
	}


	protected void onJoin(final Player player, final ArenaJoinMode joinMode) {
		if (joinMode != ArenaJoinMode.EDITING) {
			teleport(player, settings.getLobbyLocation());

			if (joinMode == ArenaJoinMode.PLAYING)
				Messenger.success(player, "Welcome in! Arena starts in " + startCountdown.getTimeLeft() + " seconds!");
			else
				Messenger.success(player, "Welcome in! Arena ends in " + heartbeat.getTimeLeft() + " seconds!");

		}

		scoreboard.onPLayerJoin(player, joinMode);
	}

	public final void leavePlayer(final Player player) {
		final ArenaPlayer cache = ArenaPlayer.getCache(player.getUniqueId());
		Valid.checkBoolean(cache.hasArena() && cache.getArena().equals(this), "Player " + player.getName() + " is not playing in " + name);

		scoreboard.onPlayerLeave(player);

		try {
			onLeave(player);
		} catch (final Throwable t) {
			Common.error(t, "Failed to properly handle onLeave for " + player.getName() + " leaving " + name);
		}

		teleport(player, cache.getJoinLocation());
		cache.markArenaLeft();

		if (!stopping) {
			players.remove(cache);
			if (players.isEmpty())
				stopArena();

		}
	}

	protected void onLeave(final Player player) {

	}

	public final ArenaPlayer findPlayer(final Player player) {
		checkIntegrity();

		for (final ArenaPlayer arenaPlayer : players)
			if (arenaPlayer.hasArena() && arenaPlayer.getArena().equals(this) && arenaPlayer.getId().equals(player.getUniqueId()))
				return arenaPlayer;

		return null;

	}

	//MASTER ARENA LOGIC

	public final void startArena() {
		Valid.checkBoolean(state == ArenaState.LOBBY, "We can only start arenas in lobby state not " + state);

		state = ArenaState.PLAYED;

		heartbeat.launch();
		scoreboard.onStart();

		if (startCountdown.isRunning())
			startCountdown.cancel();

		try {
			onStart();
		} catch (final Throwable t) {
			Common.error(t, "Failed to properly handle starting of arena " + name);
		}

		broadcastInfo("Arena " + name + " starts now! Players: " + players.size());
		Common.log("Started arena " + name);
	}

	protected void onLobbyStart() {
		Valid.checkBoolean(!startCountdown.isRunning(), "Arena is already counting to start and is in the lobby phase!");

		startCountdown.launch();
		scoreboard.onLobbyStart();
	}

	protected void onEditStart() {
		Valid.checkBoolean(!startCountdown.isRunning(), "Arena is already counting to start and is in the lobby phase!");

		scoreboard.onEditStart();
	}

	public final void stopArena() {
		Valid.checkBoolean(state != ArenaState.STOPPED, "We can only stop arenas on stopped state not " + state);

		stopping = true;

		if (state != ArenaState.EDITED) {
			if (startCountdown.isRunning())
				startCountdown.cancel();

			if (heartbeat.isRunning())
				heartbeat.cancel();

			broadcastInfo("Arena " + name + " ended now! Thank you and good bye");

			for (final Player player : getPlayers())
				leavePlayer(player);
		}

		scoreboard.onStop();

		onStop();

		try {
			onStop();
		} catch (final Throwable t) {
			Common.error(t, "Failed to properly stop the arena " + name);
		}

		state = ArenaState.STOPPED;
		players.clear();
		stopping = false;
		Common.log("Stopped arena " + name);
	}

	protected void onStart() {
	}

	protected void onStop() {
	}

	public final void broadcastInfo(final String message) {
		checkIntegrity();

		forEach(player -> Messenger.info(player, message));
	}

	public final void broadcastWarning(final String warning) {
		checkIntegrity();

		forEach(player -> Messenger.warn(player, warning));
	}

	protected final void forEach(final Consumer<Player> consumer) {
		for (final Player player : getPlayers())
			consumer.accept(player);
	}

	public final String getName() {
		return name;
	}

	public final String getType() {
		return type;
	}

	public final ArenaState getState() {
		return state;
	}

	public boolean isReady() {
		return settings.isSetup();
	}

	public boolean isStopped() {
		return state == ArenaState.STOPPED;
	}

	protected final boolean isStopping() {
		return stopping;
	}

	public final List<Player> getPlayers() {
		final List<Player> list = new ArrayList<>();

		for (final ArenaPlayer player : players)
			list.add(player.getPlayer());

		return list;
	}

	protected final List<ArenaPlayer> getArenaPlayers() {
		return Collections.unmodifiableList(players);
	}

	protected void teleport(@NonNull final Player player, final Location location) {
		Valid.checkNotNull(location, "Cannot teleport " + player.getName() + " to a null location");
		final Location topOfTheBlock = location.getBlock().getLocation().add(0.5, 1, 0.5); // tp player at the middle of the block

		player.teleport(topOfTheBlock);
	}

	// runs a few security checks to prevent accidental programming errors
	private void checkIntegrity() {
		int playing = 0, editing = 0, spectating = 0;

		for (final ArenaPlayer arenaPlayer : players) {
			final Player player = arenaPlayer.getPlayer();
			final ArenaJoinMode mode = arenaPlayer.getMode();

			Valid.checkBoolean(player != null && player.isOnline(), "Found a disconnected player " + player + " in arena " + getName());

			if (mode == ArenaJoinMode.PLAYING)
				playing++;

			else if (mode == ArenaJoinMode.EDITING)
				editing++;

			else if (mode == ArenaJoinMode.SPECTATING)
				spectating++;
		}

		if (state == ArenaState.STOPPED)
			Valid.checkBoolean(players.isEmpty(), "Found players in a stopped " + getName() + " arena: " + players);

		if (editing > 0) {
			Valid.checkBoolean(state == ArenaState.EDITED, "Arena " + getName() + " must be in EDIT mode not " + state + " while there are " + editing + " editing players!");
			Valid.checkBoolean(playing == 0 && spectating == 0, "Found " + playing + " and " + spectating + " players in edited arena " + getName());
		}
	}

	@Override
	public final boolean equals(final Object obj) {
		return obj instanceof Arena && ((Arena) obj).getName().equals(this.name);
	}

	@Override
	public final String toString() {
		return "Arena {name=" + name + "}";
	}
}