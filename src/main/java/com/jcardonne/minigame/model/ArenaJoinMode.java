package com.jcardonne.minigame.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ArenaJoinMode {

	PLAYING("playing"),

	EDITING("editing"),

	SPECTATING("spectating");

	@Getter
	private final String localized;
}
