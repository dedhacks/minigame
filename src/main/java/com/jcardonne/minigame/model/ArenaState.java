package com.jcardonne.minigame.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ArenaState {

	STOPPED("stopped"),

	LOBBY("lobby"),

	PLAYED("played"),

	EDITED("edited");

	@Getter
	private final String localized;
}
