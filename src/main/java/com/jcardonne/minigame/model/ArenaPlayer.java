package com.jcardonne.minigame.model;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.settings.YamlSectionConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ArenaPlayer extends YamlSectionConfig {

	private static final Map<UUID, ArenaPlayer> cacheMap = new HashMap<>();

	@Getter
	private final UUID id;
	private String arena;
	@Getter
	private ArenaJoinMode mode;
	@Getter
	private Location joinLocation; // before he joined the arena so we can return him back

	public ArenaPlayer(final UUID id) {
		super("Players." + id.toString());

		this.id = id;

		loadConfiguration(NO_DEFAULT, "data.db");
	}

	public Arena getArena() throws FoException {
		Valid.checkBoolean(hasArena(), "Player " + getPlayer().getName() + " does not have any arena!");

		return ArenaManager.findArena(arena);
	}

	public void markArenaJoin(final Player player, final Arena arena, final ArenaJoinMode joinMode) {
		Valid.checkBoolean(!hasArena(), "Player " + getPlayer().getName() + " already has an arena " + arena);

		this.arena = arena.getName();
		this.mode = joinMode;
		this.joinLocation = player.getLocation();
	}

	public void markArenaLeft() {
		Valid.checkBoolean(hasArena(), "Player " + getPlayer().getName() + " does not have an arena");

		this.arena = null;
		this.mode = null;
		this.joinLocation = null;
	}

	public boolean hasArena() {
		return arena != null;
	}

	public Player getPlayer() {
		return Remain.getPlayerByUUID(id);
	}

	// --------------------------------------------------------------------------------------------------------------
	// Static methods below
	// --------------------------------------------------------------------------------------------------------------

	public static ArenaPlayer getCache(final Player player) {
		return getCache(player.getUniqueId());
	}

	public static ArenaPlayer getCache(final UUID uuid) {
		ArenaPlayer cache = cacheMap.get(uuid);

		if (cache == null) {
			cache = new ArenaPlayer(uuid);

			cacheMap.put(uuid, cache);
		}

		return cache;
	}

	public static void clearAllData() {
		cacheMap.clear();
	}
}
