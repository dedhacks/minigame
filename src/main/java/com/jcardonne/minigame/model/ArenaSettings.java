package com.jcardonne.minigame.model;

import lombok.Getter;
import org.bukkit.Location;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.ConfigSerializable;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.region.Region;
import org.mineacademy.fo.settings.YamlConfig;

@Getter
public class ArenaSettings extends YamlConfig implements ConfigSerializable {

	private int maxPlayers;
	private SimpleTime gameDuration;
	private SimpleTime lobbyDuration;
	private Region region;
	private Location lobbyLocation;

	public ArenaSettings(final String arenaName) {
		setHeader(Common.configLine(), "Welcome to the main configuration for " + arenaName, Common.configLine());

		loadConfiguration(NO_DEFAULT, "arenas/" + arenaName + ".yml");

		//auto-save
		save();
	}

	@Override
	protected void onLoadFinish() {
		this.maxPlayers = getInteger("Max_Players", 20);
		this.gameDuration = getTime("Game_Duration", "10 seconds");
		this.lobbyDuration = getTime("Lobby_Duration", "5 seconds");
		this.region = get("Region", Region.class);
		this.lobbyLocation = getLocation("Lobby_Location");
	}

	public final void setMaxPlayers(final int maxPlayers) {
		this.maxPlayers = maxPlayers;
		save();
	}

	public void setGameDuration(final SimpleTime gameDuration) {
		this.gameDuration = gameDuration;
		save();
	}

	public void setLobbyDuration(final SimpleTime lobbyDuration) {
		this.lobbyDuration = lobbyDuration;
		save();
	}

	public void setRegion(final Location primary, final Location secondary) {
		if (this.region != null)
			this.region.updateLocationsWeak(primary, secondary);
		else
			this.region = new Region(primary, secondary); //foundation handle null

		save();
	}

	public void setLobby(final Location location) {
		this.lobbyLocation = location;

		save();
	}

	public boolean isSetup() {
		return region != null && region.isWhole() && lobbyLocation != null;
	}

	protected void setArenaType(final String type) {
		if (!isSet("Types"))
			save("Type", type);
	}


	@Override
	public SerializedMap serialize() {
		final SerializedMap map = new SerializedMap();
		return SerializedMap.ofArray(
				"Max_Players", maxPlayers,
				"Game_Duration", gameDuration,
				"Region", region,
				"Lobby_Duration", lobbyDuration,
				"Lobby_Location", lobbyLocation
		);
	}
}
