package com.jcardonne.minigame.model;

import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.Countdown;

public class ArenaCountdownStart extends Countdown {

	private final Arena arena;

	protected ArenaCountdownStart(final Arena arena) {
		super(arena.getSettings().getLobbyDuration());

		this.arena = arena;
	}

	@Override
	protected void onTick() { //run every second until it hit 0
		arena.broadcastWarning("Arena starts in less than " + Common.plural(getTimeLeft(), " second") + ".");
	}

	@Override
	protected void onEnd() {
		arena.startArena();
	}
}
